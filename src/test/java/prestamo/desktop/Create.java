package prestamo.desktop;

import static org.junit.Assert.*;

import org.junit.Test;

import prestamo.desktop.model.UsuarioModelo;

public class Create {

	@Test
	public void test() {
		UsuarioModelo um = new UsuarioModelo();
		um.setNombre("carlos");
		um.setApellido("barria");
		assertTrue(um.crear());
	}

}
