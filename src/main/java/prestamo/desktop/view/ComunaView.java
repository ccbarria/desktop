package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ComunaView extends JFrame {

	private JPanel contentPane;
	private JTextField nombre;
	private JTextField region_id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ComunaView frame = new ComunaView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ComunaView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblComuna = new JLabel("COMUNA");
		lblComuna.setFont(new Font("Arial", Font.BOLD, 20));
		lblComuna.setBounds(162, 11, 103, 39);
		contentPane.add(lblComuna);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(78, 84, 46, 14);
		contentPane.add(lblNewLabel);
		
		nombre = new JTextField();
		nombre.setBounds(162, 81, 249, 20);
		contentPane.add(nombre);
		nombre.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Región");
		lblNewLabel_1.setBounds(78, 135, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		region_id = new JTextField();
		region_id.setBounds(162, 132, 249, 20);
		contentPane.add(region_id);
		region_id.setColumns(10);
		
		JButton btnGrabar = new JButton("Grabar");
		btnGrabar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnGrabar.setBounds(322, 211, 89, 23);
		contentPane.add(btnGrabar);
	}

}
