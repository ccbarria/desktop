package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Font;

public class LoginView extends JFrame {

	private JPanel contentPane;
	private JTextField emailField;
	private JPasswordField passwordField;
	private JButton btnIniciarSesion;
	private JButton btnRegistrate;
	private static LoginView instancia;
	
	public static LoginView getInstancia() {
		if(instancia==null) {
			instancia = new LoginView();
		}
		return instancia;
	}
	/**
	 * Create the frame.
	 */
	private LoginView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		emailField = new JTextField();
		emailField.setBounds(131, 77, 254, 20);
		contentPane.add(emailField);
		emailField.setColumns(10);
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setBounds(31, 80, 46, 14);
		contentPane.add(lblCorreo);
		
		JLabel lblContrasea = new JLabel("Contraseña");
		lblContrasea.setBounds(31, 121, 90, 14);
		contentPane.add(lblContrasea);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(131, 118, 254, 20);
		contentPane.add(passwordField);
		
		btnIniciarSesion = new JButton("Iniciar Sesion");
		btnIniciarSesion.setBounds(97, 171, 115, 23);
		contentPane.add(btnIniciarSesion);
		
		btnRegistrate = new JButton("Registrate");
		btnRegistrate.setBounds(253, 171, 107, 23);
		contentPane.add(btnRegistrate);
		
		JLabel lblAccesoAPrestamos = new JLabel("ACCESO A PRESTAMOS");
		lblAccesoAPrestamos.setFont(new Font("Arial", Font.BOLD, 20));
		lblAccesoAPrestamos.setBounds(106, 11, 254, 23);
		contentPane.add(lblAccesoAPrestamos);
	}

	public JTextField getEmailField() {
		return emailField;
	}

	public void setEmailField(JTextField emailField) {
		this.emailField = emailField;
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}

	public void setPasswordField(JPasswordField passwordField) {
		this.passwordField = passwordField;
	}

	public JButton getBtnIniciarSesion() {
		return btnIniciarSesion;
	}

	public void setBtnIniciarSesion(JButton btnIniciarSesion) {
		this.btnIniciarSesion = btnIniciarSesion;
	}

	public JButton getBtnRegistrate() {
		return btnRegistrate;
	}

	public void setBtnRegistrate(JButton btnRegistrate) {
		this.btnRegistrate = btnRegistrate;
	}
}





