package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;

public class PagoView extends JFrame {

	private JPanel contentPane;
	private JTextField tipo;
	private JTextField monto;
	private JTextField usuarioid;
	private JTextField prestamoid;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PagoView frame = new PagoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PagoView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 476, 358);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panelPago = new JPanel();
		contentPane.add(panelPago, BorderLayout.CENTER);
		panelPago.setLayout(null);
		
		JLabel lblPagos = new JLabel("PAGOS");
		lblPagos.setFont(new Font("Arial", Font.BOLD, 20));
		lblPagos.setBounds(188, 11, 78, 28);
		panelPago.add(lblPagos);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(30, 50, 80, 20);
		panelPago.add(lblTipo);
		
		tipo = new JTextField();
		tipo.setBounds(130, 50, 200, 20);
		panelPago.add(tipo);
		tipo.setColumns(10);
		
		JLabel lblMonto = new JLabel("Monto");
		lblMonto.setBounds(30, 104, 80, 20);
		panelPago.add(lblMonto);
		
		monto = new JTextField();
		monto.setColumns(10);
		monto.setBounds(130, 104, 200, 20);
		panelPago.add(monto);
		
		JLabel lblIdUsuario = new JLabel("Id Usuario");
		lblIdUsuario.setBounds(30, 158, 80, 20);
		panelPago.add(lblIdUsuario);
		
		usuarioid = new JTextField();
		usuarioid.setColumns(10);
		usuarioid.setBounds(130, 158, 200, 20);
		panelPago.add(usuarioid);
		
		JLabel lblIdPrestamo = new JLabel("Id Prestamo");
		lblIdPrestamo.setBounds(30, 212, 80, 20);
		panelPago.add(lblIdPrestamo);
		
		prestamoid = new JTextField();
		prestamoid.setColumns(10);
		prestamoid.setBounds(130, 212, 200, 20);
		panelPago.add(prestamoid);
		
		JButton btnGrabar = new JButton("Grabar");
		btnGrabar.setBounds(351, 211, 89, 23);
		panelPago.add(btnGrabar);
	}
}
