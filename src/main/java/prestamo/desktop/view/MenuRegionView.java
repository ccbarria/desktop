package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuRegionView extends JFrame {

	private JPanel contentPane;
	private JButton ingresarButton;
	private JButton modificarButton;
	private JButton listarButton;
	private JButton retornarButton;
	private static MenuRegionView instancia;
	
	public static MenuRegionView getInstancia() {

		if(instancia==null) {
			instancia = new MenuRegionView();
		}
		return instancia;
	}
	/**
	 * Create the frame.
	 */
	private MenuRegionView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRegiones = new JLabel("REGIONES");
		lblRegiones.setFont(new Font("Arial", Font.BOLD, 25));
		lblRegiones.setBounds(152, 11, 148, 48);
		contentPane.add(lblRegiones);
		
		ingresarButton = new JButton("Ingresar");
		ingresarButton.setBounds(14, 119, 89, 23);
		contentPane.add(ingresarButton);
		
		modificarButton = new JButton("Modificar");
		modificarButton.setBounds(113, 119, 89, 23);
		contentPane.add(modificarButton);
		
		listarButton = new JButton("Listar");
		listarButton.setBounds(212, 119, 89, 23);
		contentPane.add(listarButton);
		
		retornarButton = new JButton("<-- Retornar");
		retornarButton.setBounds(311, 119, 113, 23);
		contentPane.add(retornarButton);
	}
	public JButton getIngresarButton() {
		return ingresarButton;
	}
	public JButton getModificarButton() {
		return modificarButton;
	}
	public JButton getListarButton() {
		return listarButton;
	}
	public JButton getRetornarButton() {
		return retornarButton;
	}
	
	
	
}
