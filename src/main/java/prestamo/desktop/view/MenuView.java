package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;

public class MenuView extends JFrame {

	private JPanel contentPane;
	private JMenuItem mntmSolicitar;
	private JMenuItem mntmRegiones;
	
	/**
	 * Create the frame.
	 */
	public MenuView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 424, 21);
		contentPane.add(menuBar);
		
		JMenu mnPrestamo = new JMenu("Prestamo");
		menuBar.add(mnPrestamo);
		
		mntmSolicitar = new JMenuItem("Solicitar");
		mnPrestamo.add(mntmSolicitar);
		
		JMenuItem mntmVerificar = new JMenuItem("Verificar");
		mnPrestamo.add(mntmVerificar);
		
		JMenu mnMantenedores = new JMenu("Mantenedores");
		menuBar.add(mnMantenedores);
		
		mntmRegiones = new JMenuItem("Regiones");
		
		mnMantenedores.add(mntmRegiones);
		
		JMenuItem mntmComunas = new JMenuItem("Comunas");
		mnMantenedores.add(mntmComunas);
		
		JMenuItem mntmPrestamistas = new JMenuItem("Prestamistas");
		mnMantenedores.add(mntmPrestamistas);
		
		JMenuItem mntmReputacion = new JMenuItem("Reputacion");
		mnMantenedores.add(mntmReputacion);
		
		JMenuItem mntmPrestatarios = new JMenuItem("Prestatarios");
		mnMantenedores.add(mntmPrestatarios);
		
		JMenu mnDepositos = new JMenu("Depositos");
		menuBar.add(mnDepositos);
		
		JMenuItem mntmDeposito = new JMenuItem("Deposito");
		mnDepositos.add(mntmDeposito);
		
		JMenu mnSalir = new JMenu("Salir");
		menuBar.add(mnSalir);
		
		
	}

	public JMenuItem getMntmSolicitar() {
		return mntmSolicitar;
	}

	public JMenuItem getMntmRegiones() {
		return mntmRegiones;
	}
	
}
