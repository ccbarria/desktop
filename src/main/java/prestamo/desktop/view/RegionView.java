package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;

public class RegionView extends JFrame {

	private JPanel contentPane;
	private JTextField nombre;
	private JButton btnGrabar;

	/**
	 * Create the frame.
	 */
	public RegionView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panelRegion = new JPanel();
		contentPane.add(panelRegion, BorderLayout.CENTER);
		panelRegion.setLayout(null);
		
		JLabel lblRegiones = new JLabel("REGIONES");
		lblRegiones.setFont(new Font("Arial", Font.BOLD, 20));
		lblRegiones.setBounds(134, 18, 171, 58);
		panelRegion.add(lblRegiones);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(39, 87, 64, 20);
		panelRegion.add(lblNombre);
		
		nombre = new JTextField();
		nombre.setBounds(103, 87, 293, 20);
		panelRegion.add(nombre);
		nombre.setColumns(10);
		System.out.println(nombre);
		
		this.btnGrabar = new JButton("Grabar");
		this.btnGrabar.setBounds(307, 180, 89, 23);
		panelRegion.add(btnGrabar);
		
	}

	public JButton getBtnGrabar() {
		return btnGrabar;
	}

	public void setBtnGrabar(JButton btnGrabar) {
		this.btnGrabar = btnGrabar;
	}

	public JTextField getNombre() {
		return nombre;
	}

	public void setNombre(JTextField nombre) {
		this.nombre = nombre;
	}
	
	
	
}
