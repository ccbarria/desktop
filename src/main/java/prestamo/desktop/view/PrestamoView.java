package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;

public class PrestamoView extends JFrame {
	private JTextField motivo;
	private JTextField fecha;
	private JTextField monto;
	private JTextField cuotas;
	private JTextField usuario_id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrestamoView frame = new PrestamoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrestamoView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 438);
		getContentPane().setLayout(null);
		
		JLabel lblPrestamo = new JLabel("PRESTAMO");
		lblPrestamo.setFont(new Font("Arial", Font.BOLD, 20));
		lblPrestamo.setBounds(136, 6, 188, 53);
		getContentPane().add(lblPrestamo);
		
		JLabel lblNewLabel = new JLabel("Motivo");
		lblNewLabel.setBounds(52, 122, 46, 20);
		getContentPane().add(lblNewLabel);
		
		motivo = new JTextField();
		motivo.setBounds(136, 122, 188, 20);
		getContentPane().add(motivo);
		motivo.setColumns(10);
		
		JLabel lblMonto = new JLabel("Monto");
		lblMonto.setBounds(52, 173, 46, 20);
		getContentPane().add(lblMonto);
		
		JLabel lblCuotas = new JLabel("Cuotas");
		lblCuotas.setBounds(52, 229, 46, 20);
		getContentPane().add(lblCuotas);
		
		JLabel lblFecha = new JLabel("Fecha");
		lblFecha.setBounds(52, 73, 46, 20);
		getContentPane().add(lblFecha);
		
		JLabel lblIdUsuario = new JLabel("Usuario");
		lblIdUsuario.setBounds(51, 281, 75, 20);
		getContentPane().add(lblIdUsuario);
		
		fecha = new JTextField();
		fecha.setColumns(10);
		fecha.setBounds(136, 70, 188, 20);
		getContentPane().add(fecha);
		
		monto = new JTextField();
		monto.setColumns(10);
		monto.setBounds(136, 170, 188, 20);
		getContentPane().add(monto);
		
		cuotas = new JTextField();
		cuotas.setColumns(10);
		cuotas.setBounds(136, 226, 188, 20);
		getContentPane().add(cuotas);
		
		usuario_id = new JTextField();
		usuario_id.setColumns(10);
		usuario_id.setBounds(136, 278, 188, 20);
		getContentPane().add(usuario_id);
		
		JButton btnNewButton = new JButton("Grabar");
		btnNewButton.setBounds(335, 318, 89, 23);
		getContentPane().add(btnNewButton);
	}
}
