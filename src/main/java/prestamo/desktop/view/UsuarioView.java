package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import prestamo.desktop.model.ComunaModel;

public class UsuarioView extends JFrame {

	private JPanel contentPane;
	
	
	private JButton btnGrabar;
	private JTextField clave;
	private JTextField rut;
	private JTextField nombre;
	private JTextField apellido;
	private JTextField correo;
	private JTextField tipo;
	private JTextField calle;
	private JTextField numerocasa;
	private JTextField reputacionid;
	private JTextField comunaid;
	
    
    ComunaModel cm = new ComunaModel();
    
	/**
	 * Create the frame.
	 */
	public UsuarioView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 522, 520);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panelUsuario = new JPanel();
		contentPane.add(panelUsuario, BorderLayout.CENTER);
		panelUsuario.setLayout(null);
		
		JLabel lblUsuarios = new JLabel("USUARIOS");
		lblUsuarios.setFont(new Font("Arial", Font.BOLD, 20));
		lblUsuarios.setBounds(183, 11, 117, 22);
		panelUsuario.add(lblUsuarios);
		
		JLabel lblRut = new JLabel("Rut");
		lblRut.setBounds(30, 77, 80, 20);
		panelUsuario.add(lblRut);
		
		rut = new JTextField();
		rut.setBounds(130, 77, 200, 20);
		panelUsuario.add(rut);
		rut.setColumns(10);
		
		btnGrabar = new JButton("Grabar");
		btnGrabar.setBounds(342, 407, 89, 23);
		panelUsuario.add(btnGrabar);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(30, 114, 80, 20);
		panelUsuario.add(lblNombre);
		
		nombre = new JTextField();
		nombre.setColumns(10);
		nombre.setBounds(130, 114, 200, 20);
		panelUsuario.add(nombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(30, 151, 80, 20);
		panelUsuario.add(lblApellido);
		
		apellido = new JTextField();
		apellido.setColumns(10);
		apellido.setBounds(130, 151, 200, 20);
		panelUsuario.add(apellido);
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setBounds(30, 188, 80, 20);
		panelUsuario.add(lblCorreo);
		
		correo = new JTextField();
		correo.setColumns(10);
		correo.setBounds(130, 188, 200, 20);
		panelUsuario.add(correo);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(30, 225, 80, 20);
		panelUsuario.add(lblTipo);
		
		tipo = new JTextField();
		tipo.setColumns(10);
		tipo.setBounds(130, 225, 200, 20);
		panelUsuario.add(tipo);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(30, 262, 80, 20);
		panelUsuario.add(lblCalle);
		
		JLabel lblNumeroCasa = new JLabel("Numero Casa");
		lblNumeroCasa.setBounds(30, 299, 80, 20);
		panelUsuario.add(lblNumeroCasa);
		
		JLabel lblIdComuna = new JLabel("Id Comuna");
		lblIdComuna.setBounds(30, 373, 80, 20);
		panelUsuario.add(lblIdComuna);
		
		JLabel lblIdReputacion = new JLabel("Id Reputacion");
		lblIdReputacion.setBounds(30, 336, 80, 20);
		panelUsuario.add(lblIdReputacion);
		
		JLabel lblClave = new JLabel("Clave");
		lblClave.setBounds(30, 410, 80, 20);
		panelUsuario.add(lblClave);
		
		numerocasa = new JTextField();
		numerocasa.setColumns(10);
		numerocasa.setBounds(130, 299, 200, 20);
		panelUsuario.add(numerocasa);
		
		comunaid = new JTextField();
		comunaid.setColumns(10);
		comunaid.setBounds(130, 373, 200, 20);
		panelUsuario.add(comunaid);
		
		clave = new JTextField();
		clave.setColumns(10);
		clave.setBounds(130, 410, 200, 20);
		panelUsuario.add(clave);
		
		calle = new JTextField();
		calle.setColumns(10);
		calle.setBounds(130, 262, 200, 20);
		panelUsuario.add(calle);
		
		reputacionid = new JTextField();
		reputacionid.setColumns(10);
		reputacionid.setBounds(130, 336, 200, 20);
		panelUsuario.add(reputacionid);
		
		JComboBox comboBoxComuna = new JComboBox();
		comboBoxComuna.setBounds(354, 376, 150, 20);
		panelUsuario.add(comboBoxComuna);
		
		
		cm.llenaCombo(comboBoxComuna);
	}

	public JButton getBtnGrabar() {
		return btnGrabar;
	}

	public void setBtnGrabar(JButton btnGrabar) {
		this.btnGrabar = btnGrabar;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	

	public JTextField getClave() {
		return clave;
	}

	public void setClave(JTextField clave) {
		this.clave = clave;
	}

	public JTextField getRut() {
		return rut;
	}

	public void setRut(JTextField rut) {
		this.rut = rut;
	}

	public JTextField getNombre() {
		return nombre;
	}

	public void setNombre(JTextField nombre) {
		this.nombre = nombre;
	}

	public JTextField getApellido() {
		return apellido;
	}

	public void setApellido(JTextField apellido) {
		this.apellido = apellido;
	}

	public JTextField getCorreo() {
		return correo;
	}

	public void setCorreo(JTextField correo) {
		this.correo = correo;
	}

	public JTextField getTipo() {
		return tipo;
	}

	public void setTipo(JTextField tipo) {
		this.tipo = tipo;
	}

	public JTextField getCalle() {
		return calle;
	}

	public void setCalle(JTextField calle) {
		this.calle = calle;
	}

	public JTextField getNumerocasa() {
		return numerocasa;
	}

	public void setNumerocasa(JTextField numerocasa) {
		this.numerocasa = numerocasa;
	}

	public JTextField getReputacionid() {
		return reputacionid;
	}

	public void setReputacionid(JTextField reputacionid) {
		this.reputacionid = reputacionid;
	}

	public JTextField getComunaid() {
		return comunaid;
	}

	public void setComunaid(JTextField comunaid) {
		this.comunaid = comunaid;
	}
}
