package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class CuotaView extends JFrame {

	private JPanel contentPane;
	private JTextField tipo;
	private JTextField monto;
	private JTextField usuarioid;
	private JTextField prestamoid;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CuotaView frame = new CuotaView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CuotaView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 480, 368);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panelCuota = new JPanel();
		contentPane.add(panelCuota, BorderLayout.CENTER);
		panelCuota.setLayout(null);
		
		JLabel lblCuotas = new JLabel("CUOTAS");
		lblCuotas.setFont(new Font("Arial", Font.BOLD, 20));
		lblCuotas.setBounds(162, 11, 100, 34);
		panelCuota.add(lblCuotas);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(30, 58, 80, 20);
		panelCuota.add(lblTipo);
		
		tipo = new JTextField();
		tipo.setBounds(130, 58, 200, 20);
		panelCuota.add(tipo);
		tipo.setColumns(10);
		
		JLabel lblMonto = new JLabel("Monto");
		lblMonto.setBounds(30, 112, 80, 20);
		panelCuota.add(lblMonto);
		
		JLabel lblIdUsuario = new JLabel("Id Usuario");
		lblIdUsuario.setBounds(30, 166, 80, 20);
		panelCuota.add(lblIdUsuario);
		
		JLabel lblIdPrestamo = new JLabel("Id Prestamo");
		lblIdPrestamo.setBounds(30, 220, 80, 20);
		panelCuota.add(lblIdPrestamo);
		
		monto = new JTextField();
		monto.setColumns(10);
		monto.setBounds(130, 112, 200, 20);
		panelCuota.add(monto);
		
		usuarioid = new JTextField();
		usuarioid.setColumns(10);
		usuarioid.setBounds(130, 166, 200, 20);
		panelCuota.add(usuarioid);
		
		prestamoid = new JTextField();
		prestamoid.setColumns(10);
		prestamoid.setBounds(130, 220, 200, 20);
		panelCuota.add(prestamoid);
		
		JButton btnNewButton = new JButton("Grabar");
		btnNewButton.setBounds(335, 219, 89, 23);
		panelCuota.add(btnNewButton);
	}
}
