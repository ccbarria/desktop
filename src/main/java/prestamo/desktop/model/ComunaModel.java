package prestamo.desktop.model;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;


public class ComunaModel implements Modelo<ComunaModel>{
	private String id;
	private String nombre;
	private String regionId;
	@Override
	public boolean crear() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean borrar() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean actualizar() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public ComunaModel get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void llenaCombo(JComboBox comboBoxComuna){
     	ResultSet resultado = null;
		String SSQL = "SELECT nombre FROM Comuna ORDER BY nombre ASC";
		Connection con = DB.getConnection();
	try {
		PreparedStatement ps = con.prepareStatement(SSQL);
		resultado = ps.executeQuery();
		comboBoxComuna.addItem("Seleccione una opción");
			
		   while(resultado.next()){
		 	   comboBoxComuna.addItem(resultado.getString("nombre"));
		 	  	}		   
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		JOptionPane.showMessageDialog(null, e+"******");
	}
	}
}   	
