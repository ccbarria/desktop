package prestamo.desktop.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RegionModelo implements Modelo<RegionModelo> {
	
	private Integer id;
	private String nombre;
	
	
	@Override
	public boolean crear() {
		boolean resultado = false;
		String Sql = "INSERT INTO Region(nombre) VALUES( ? );";
		Connection con = DB.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement(Sql);
			ps.setString(1, this.nombre);
			resultado = ps.executeUpdate()==1;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
	}

	@Override
	public boolean borrar() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean actualizar() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public RegionModelo get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}



