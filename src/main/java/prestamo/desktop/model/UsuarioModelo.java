package prestamo.desktop.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioModelo implements Modelo<UsuarioModelo>{
	
	private int id;
	private String rut;
	private String nombre;
	private String apellido;
	private String correo;
	private String tipo;
	private String calle;
	private String numeroCasa;
	private int reputacion_id;
	private int comuna_id;
	private String clave;
	
	@Override
	public boolean crear() {
		// TODO Auto-generated method stub
		boolean resultado = false;
		String Sql = "INSERT INTO Usuario(rut,nombre,apellido,correo,tipo,calle,numeroCasa,reputacion_id,comuna_id,clave)"
				+ " VALUES( ?,?,?,?,?,?,?,?,?,? );";
		Connection con = DB.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement(Sql);
			ps.setString(1, this.rut);
			ps.setString(2, this.nombre);
			ps.setString(3, this.apellido);
			ps.setString(4, this.correo);
			ps.setString(5, this.tipo);
			ps.setString(6, this.calle);
			ps.setString(7, this.numeroCasa);
			ps.setInt(8, this.reputacion_id);
			ps.setInt(9, this.comuna_id);
			ps.setString(10, this.clave);
			resultado = ps.executeUpdate()==1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
	}




	@Override
	public boolean borrar() {
		// TODO Auto-generated method stub
		return false;
	}




	@Override
	public boolean actualizar() {
		// TODO Auto-generated method stub
		return false;
	}




	@Override
	public UsuarioModelo get(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	

	public boolean autenticate(String email, String pass) {
		boolean resultado = false;
		String sql = "SELECT * FROM Usuario WHERE correo = ? AND clave = ?";
		Connection con = DB.getConnection();
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, email);
			ps.setString(2, pass);
			ResultSet rs = ps.executeQuery();
			resultado = rs.next();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
	}




	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}




	public String getRut() {
		return rut;
	}




	public void setRut(String rut) {
		this.rut = rut;
	}




	public String getNombre() {
		return nombre;
	}




	public void setNombre(String nombre) {
		this.nombre = nombre;
	}




	public String getApellido() {
		return apellido;
	}




	public void setApellido(String apellido) {
		this.apellido = apellido;
	}




	public String getCorreo() {
		return correo;
	}




	public void setCorreo(String correo) {
		this.correo = correo;
	}




	public String getTipo() {
		return tipo;
	}




	public void setTipo(String tipo) {
		this.tipo = tipo;
	}




	public String getCalle() {
		return calle;
	}




	public void setCalle(String calle) {
		this.calle = calle;
	}




	public String getNumeroCasa() {
		return numeroCasa;
	}




	public void setNumeroCasa(String numeroCasa) {
		this.numeroCasa = numeroCasa;
	}






	public int getReputacion_id() {
		return reputacion_id;
	}




	public void setReputacion_id(int reputacion_id) {
		this.reputacion_id = reputacion_id;
	}




	public int getComuna_id() {
		return comuna_id;
	}




	public void setComuna_id(int comuna_id) {
		this.comuna_id = comuna_id;
	}




	public String getClave() {
		return clave;
	}




	public void setClave(String clave) {
		this.clave = clave;
	}




	



	
	
}

