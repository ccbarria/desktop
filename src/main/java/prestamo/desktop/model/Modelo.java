
package prestamo.desktop.model;

public interface Modelo <T> {
	public abstract boolean crear();
	public abstract boolean borrar();
	public abstract boolean actualizar();
	public abstract T get(int id);
}
