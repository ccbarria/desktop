package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;


import prestamo.desktop.model.RegionModelo;
import prestamo.desktop.view.RegionView;

public class RegionController implements ActionListener{
	
	RegionView rf;

	RegionController(RegionView rf){
		this.rf = rf;
		rf.getBtnGrabar().addActionListener(this);
		}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if(e.getSource()==rf.getBtnGrabar()) {
			String nombre = rf.getNombre().getText();
			
			if(!(nombre.equals(""))) {
				RegionModelo rm = new RegionModelo();
				rm.setNombre(nombre);
				if(rm.crear()) {
					JOptionPane.showMessageDialog(null, "Se registro con exito");
				}
			}else{
				JOptionPane.showMessageDialog(null, "Debe Ingresar algun campo");
			}
		}
		
		
	}
		
}
