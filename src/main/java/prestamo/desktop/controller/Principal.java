//package prestamo.desktop.controller;
//import prestamo.desktop.model.RegionModelo;
//import prestamo.desktop.view.RegionView;
//public class Principal {
//	
//	public static void main(String[] args) {
//		
//		RegionView rf = new RegionView();
//		
//		RegionController rc = new RegionController(rf);
//		
//		rf.setTitle("Registrar una Region");
//		
//		rf.setVisible(true);
//		
//		RegionModelo rm = new RegionModelo();
//		
//		rm.crear();
//		
//	}
//
//}



package prestamo.desktop.controller;

import prestamo.desktop.view.LoginView;
import prestamo.desktop.view.MenuView;

public class Principal {
	public static void main(String[] args) {
		
		LoginView lv = LoginView.getInstancia();
		LoginController lc = new LoginController(lv);
		lv.setExtendedState(LoginView.MAXIMIZED_BOTH);
		lv.setVisible(true);
	}
	

}
