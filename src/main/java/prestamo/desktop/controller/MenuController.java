package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.view.LoginView;
import prestamo.desktop.view.MenuRegionView;
import prestamo.desktop.view.MenuView;
import prestamo.desktop.view.RegionView;

public class MenuController implements ActionListener{
	private MenuView view;
	
	MenuController(MenuView view){
		
		this.view = view;
		this.view.getMntmSolicitar().addActionListener(this);
		this.view.getMntmRegiones().addActionListener(this);
		
		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==view.getMntmSolicitar()) {
			view.setVisible(false);
		}else if(e.getSource()== this.view.getMntmRegiones()) {
			
			view.setVisible(false);
			MenuRegionView menuRegionView = MenuRegionView.getInstancia();
			menuRegionView.setVisible(true);
		}
	
	}
	
	

}
