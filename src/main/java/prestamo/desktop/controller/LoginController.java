package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import prestamo.desktop.model.UsuarioModelo;
import prestamo.desktop.view.LoginView;
import prestamo.desktop.view.MenuView;
import prestamo.desktop.view.UsuarioView;

public class LoginController implements ActionListener{
	public static final String MAXIMIZED_BOTH = null;
	LoginView view;
	
	LoginController(LoginView view){
		this.view = view;
		this.view.getBtnIniciarSesion().addActionListener(this);
		this.view.getBtnRegistrate().addActionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		// TODO Auto-generated method stub
		if(e.getSource()== view.getBtnIniciarSesion()) {
			String email = view.getEmailField().getText();
			String pass = view.getPasswordField().getText();
			UsuarioModelo um = new UsuarioModelo();
			if(um.autenticate(email, pass)) {
				view.setVisible(false);
				MenuView menuView = new MenuView();
				MenuController mc = new MenuController(menuView);
				menuView.setExtendedState(MenuView.MAXIMIZED_BOTH);
				menuView.setVisible(true);
				
			}else {
				JOptionPane.showMessageDialog(null, "hay algun problema con tu usuario o contraseña ");
			}
			
		}else if(e.getSource()== view.getBtnRegistrate()) {
			UsuarioView uv = new UsuarioView();
			UsuarioController uc = new UsuarioController(uv);
			uv.setExtendedState(UsuarioView.MAXIMIZED_BOTH);
			uv.setVisible(true);
	//		view.setVisible(false);
		}
	}

	
	
	

}
