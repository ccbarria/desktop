package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import prestamo.desktop.model.UsuarioModelo;
import prestamo.desktop.view.UsuarioView;

public class UsuarioController implements ActionListener {
		
	UsuarioView uv;
	UsuarioController(UsuarioView uv){
		this.uv = uv;
		uv.getBtnGrabar().addActionListener(this);
		}

	
	
	@Override
	public void actionPerformed(ActionEvent u) {
		// TODO Auto-generated method stub
		
		if(u.getSource()==uv.getBtnGrabar()) {
			
			String rut = uv.getRut().getText();
			String nombre = uv.getNombre().getText();
			String apellido = uv.getApellido().getText();
			String correo = uv.getCorreo().getText();
			String tipo = uv.getTipo().getText();
			String calle = uv.getCalle().getText();
			String numerocasa = uv.getNumerocasa().getText();
			String clave = uv.getClave().getText();
			String ri = uv.getReputacionid().getText();
			int reputacionid = Integer.parseInt(ri);
			String ci = uv.getComunaid().getText();
			int comunaid = Integer.parseInt(ci);
			
			if(!(nombre.equals(""))) {
				UsuarioModelo um = new UsuarioModelo();
				um.setRut(rut);
				um.setNombre(nombre);
				um.setApellido(apellido);
				um.setCorreo(correo);
				um.setTipo(tipo);
				um.setCalle(calle);
				um.setNumeroCasa(numerocasa);
				um.setReputacion_id(reputacionid);
				um.setComuna_id(comunaid);
				um.setClave(clave);
				if(um.crear()) {
					JOptionPane.showMessageDialog(null, "Usuario se Registro con Exito");
//					UsuarioView uv = new UsuarioView();
					uv.setVisible(false);

				}
			}else{
				JOptionPane.showMessageDialog(null, "Debe Ingresar algun campo");
			}
		}
		
	}

}
