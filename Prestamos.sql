/*creacion tabla Prestamistas*/
CREATE table PRESTAMISTAS (
	RutPrestamista varchar(15) primary key,
	NombrePrestamista varchar(255),
	)


/*creacion tabla Depositos*/
CREATE table DEPOSITOS (
	NroDeposito int identity primary key,
	FechaDeposito date,
	RutPrestamista varchar(15),
	MontoDeposito int,
	TasaInteres decimal(4,2),
	RutCliente varchar(15),
	NroPrestamo int,
	)

/* creacion tabla clientes*/
CREATE table CLIENTES (
	RutCliente varchar(15) primary key,
	NombreCliente varchar(255),
	ApellidoCliente varchar(255),
	Correo varchar (50),
	Direccion varchar(255),
	CodCiudad int,
	TipoClasificacion char,
	)

/* creacion tabla clasificacion*/
CREATE table CLASIFICACION (
    TipoClasificacion char primary key,
	Tasa decimal (4,2),
	)


	/* creacion tabla ciudades*/
CREATE table CIUDADES (
    CodCiudad int identity primary key,
	Nombre varchar (30),
	idRegion int,
	)


	/* creacion tabla usuarios*/
CREATE table USUARIOS (
    CodUsuario    varchar (25) primary key,
	NombreUsuario varchar (50),
	TipoUsuario   int,
	Password      varchar (25),
	)

	

	/* creacion tabla regiones*/
    CREATE table REGION (
    id int identity primary key,
	nombre varchar (100),
	)


/*tabla de paso para generar compartamiento de clientes*/
CREATE table COMPORTAMIENTO (
	RutCliente varchar(15) primary key,
	TotalPrestamos int,
	TotalCuotas int,
	CantidadCuotasPagadas int,
	CantidadAtrasos int,
	TipoClasificacion char,
	)


/* creacion tabla prestamos*/
CREATE table PRESTAMOS (
    NroPrestamo int identity primary key,
	FechaPrestamo date,
	RutCliente varchar(15),
	NroDeposito int,
	MontoCapital int,
	MontoTotal int,
	ValorCuota int,
	PrimerVencimiento date,
	UltimoVencimiento date,
	NroCuotas int,
	CuotaPendiente int,
	Tasa decimal (4,2),
	Saldo int,
)

/* creacion tabla CuotasPrestamo*/
CREATE table CUOTASPRESTAMO (
    id int identity primary key,
	NroPrestamo int,
	NroCuota int,
	FechaVencimiento date,
	MontoCuota int,
	SituacionCuota int
	)


/* creacion tabla Pagos*/
CREATE table PAGOS (
    NroPago int identity primary key,
	FechaPago date,
	RutCliente varchar(15),
	NroPrestamo int,
	NroCuota int,
	FechaVencimiento date,
	MontoPago int,
	)


/* elimina tabla clasificacion*/
drop table CLASIFICACION;
drop table PRESTAMOS
drop table CLIENTES;
drop table CUOTASPRESTAMO;
DROP TABLE PRESTAMISTAS;
DROP TABLE DEPOSITOS;
drop table ciudades;
drop table comportamiento
drop table Pagos;
drop table Region;
drop table Usuarios;






/*ingreso datos prestamistas*/
INSERT INTO PRESTAMISTAS VALUES('11111111-1', 'Soto Cristian');
INSERT INTO PRESTAMISTAS VALUES('22222222-2', 'Riquelme Angelica');
INSERT INTO PRESTAMISTAS VALUES('33333333-3', 'Perez Rodrigo');

select * from prestamistas



/*ingreso datos depositos*/
INSERT INTO DEPOSITOS VALUES('2018-12-18', '11111111-1', 300000, 10.00, ' ',0);
INSERT INTO DEPOSITOS VALUES('2018-12-18', '22222222-2', 500000, 10.00, ' ',0);
INSERT INTO DEPOSITOS VALUES('2018-12-18', '33333333-3', 800000, 10.00, ' ',0);

select * from depositos

/*ingreso datos tabla clientes*/
INSERT INTO CLIENTES VALUES ('44444444-4', 'Jorge', 'Soto', 'jsoto@gmail.com', 'Picarte 1500', 1, 'C')
INSERT INTO CLIENTES VALUES ('55555555-5', 'Angel', 'Carcamo', 'acarcamo@gmail.com', 'Anibal Pinto 500', 1, 'C')
INSERT INTO CLIENTES VALUES ('66666666-6', 'Ana', 'Mansilla', 'amansilla@gmail.com', 'Balmaceda 360', 2, 'C')

select * from clientes


INSERT INTO Usuarios VALUES ('carlos', 'Carlos Barria', 1, 'ingreso');
INSERT INTO Usuarios VALUES ('Roberto', 'Roberto Caceres', 2, 'ingreso');

select * from Usuarios



/*ingreso datos tabla clasificacion*/
INSERT INTO CLASIFICACION VALUES('A', 1.25);
INSERT INTO CLASIFICACION VALUES('B', 2.50);
INSERT INTO CLASIFICACION VALUES('C', 3.70);
INSERT INTO CLASIFICACION VALUES('D', 5.00);

select * from clasificacion


/*ingreso datos tabla regiones*/
INSERT INTO REGION VALUES ('I Tarapaca');
INSERT INTO REGION VALUES ('II Antofagasta');
INSERT INTO REGION VALUES ('III Atacama');
INSERT INTO REGION VALUES ('IV Coquimbo');
INSERT INTO REGION VALUES ('V Valpara�so');
INSERT INTO REGION VALUES ('VI Libertador General Bernardo O�Higgins');
INSERT INTO REGION VALUES ('VII Maule');
INSERT INTO REGION VALUES ('VIII Concepcion');
INSERT INTO REGION VALUES ('IX Araucania');
INSERT INTO REGION VALUES ('X Los Lagos');
INSERT INTO REGION VALUES ('XI Ays�n del General Carlos Iba�ez del Campo');
INSERT INTO REGION VALUES ('XII Magallanes y de la Ant�rtica Chilena');
INSERT INTO REGION VALUES ('METROPOLITANA Santiago');
INSERT INTO REGION VALUES ('XIV Los R�os');
INSERT INTO REGION VALUES ('XV Arica y Parinacota');
INSERT INTO REGION VALUES ('XVI �uble');

select * from region



/*ingreso datos tabla ciudades*/
INSERT INTO CIUDADES VALUES ('Valdivia', 10)
INSERT INTO CIUDADES VALUES ('Panguipulli', 10)
INSERT INTO CIUDADES VALUES ('La Union', 10)
INSERT INTO CIUDADES VALUES ('Temuco', 10)
INSERT INTO CIUDADES VALUES ('Osorno', 10)

select * from CIUDADES order by nombre

UPDATE CIUDADES SET Nombre = 'Talca' WHERE CodCiudad=16
UPDATE CIUDADES SET Nombre = 'Frutillar' WHERE CodCiudad=7
UPDATE CIUDADES SET Nombre = 'Puerto Montt' WHERE CodCiudad=8


SELECT TOP 1000 CodCiudad, nombre FROM Ciudades;

select * from prestamistas
select * from depositos
select * from clientes
select * from clasificacion
select * from CIUDADES
select * from comportamiento
select * from prestamos
select * from CuotasPrestamo
select * from Pagos



/*
       /*relacion entre tabla clientes y clasificacion*/
       ALTER TABLE clientes ADD FOREIGN KEY (tipo_clasificacion) REFERENCES clasificacion(tipo);
*/


/*=====================================================================================================*/
/*ingreso de un prestamo*/
/*se asigna nro deposito, total cuotas y rut cliente, se rescata capital, tasa y se calcula valor cuota*/
/*=====================================================================================================*/
/*valor cuota sera igual a: capital por tasa, ese valor por nro cuotas, se agrega al capital y
se se divide por nro cuotas*/
/*=====================================================================================================*/
declare @NroDeposito int;
declare @capital int;
declare @tasa decimal (4,2);
declare @totalCuotas int;
declare @rut varchar(15);
declare @valorCuota int;
set @NroDeposito=1;
set @capital = (select MontoDeposito from depositos where NroDeposito=@NroDeposito);
select @capital;
set @totalCuotas=10;
set @rut='44444444-4';
select clientes.nombrecliente, clientes.apellidocliente, clasificacion.tasa from clientes inner join clasificacion on clientes.tipoclasificacion=clasificacion.tipoclasificacion where clientes.rutcliente=@rut;
set @tasa=(select clasificacion.tasa from clientes inner join clasificacion on clientes.tipoclasificacion=clasificacion.tipoclasificacion where clientes.rutcliente=@rut);
select @tasa;
set @valorCuota=((@capital*(@tasa/100))*@totalCuotas);
set @valorCuota=(@valorCuota + @capital)/@totalCuotas;
select @valorCuota;
/*=====================================================================================================*/
/*agrega registro a tabla prestamos*/
/*ademas se actualiza tabla Depositos, los campos Rut Cliente y Nro Prestamo*/
declare @fechaPrestamo date = sysdatetime();
insert into PRESTAMOS values (@fechaPrestamo,@rut,@NroDeposito,@capital, (@valorCuota*@totalCuotas),@valorCuota,dateadd(month,1,@fechaPrestamo),dateadd(month,@totalCuotas,@fechaPrestamo),@totalCuotas,1,@tasa,(@valorCuota*@totalCuotas));
declare @nroPrestamo int;
set @nroPrestamo=(select max(NroPrestamo) from PRESTAMOS);
UPDATE DEPOSITOS SET RutCliente = @rut WHERE NroDeposito=@NroDeposito
UPDATE DEPOSITOS SET NroPrestamo = @nroPrestamo WHERE NroDeposito=@NroDeposito
/*=====================================================================================================*/
/*agrega registros a tabla cuotasPrestamo*/
declare @nroCuota int;
declare @fechaVencimiento date;
set @fechaVencimiento=dateadd(month,1,@fechaPrestamo);
set @nroCuota = 1;
WHILE @nroCuota < (@totalCuotas + 1)
BEGIN
	INSERT INTO CuotasPrestamo VALUES (@nroPrestamo, @nroCuota, @fechaVencimiento, @valorCuota,0);
	set @fechaVencimiento = dateadd(month,1,@fechaVencimiento);
	set @nroCuota = @nroCuota + 1;
END



/*ingreso de nuevo prestamo*/
declare @NroDeposito int;
declare @capital int;
declare @tasa decimal (4,2);
declare @totalCuotas int;
declare @rut varchar(15);
declare @valorCuota int;
set @NroDeposito=2;
set @capital = (select MontoDeposito from depositos where NroDeposito=@NroDeposito);
select @capital;
set @totalCuotas=6;
set @rut='55555555-5';
select clientes.nombrecliente, clientes.apellidocliente, clasificacion.tasa from clientes inner join clasificacion on clientes.tipoclasificacion=clasificacion.tipoclasificacion where clientes.rutcliente=@rut;
set @tasa=(select clasificacion.tasa from clientes inner join clasificacion on clientes.tipoclasificacion=clasificacion.tipoclasificacion where clientes.rutcliente=@rut);
select @tasa;
set @valorCuota=((@capital*(@tasa/100))*@totalCuotas);
set @valorCuota=(@valorCuota + @capital)/@totalCuotas;
select @valorCuota;
/*=====================================================================================================*/
/*agrega registro a tabla prestamos*/
/*ademas se actualiza tabla Depositos, los campos Rut Cliente y Nro Prestamo*/
declare @fechaPrestamo date = sysdatetime();
insert into PRESTAMOS values (@fechaPrestamo,@rut,@NroDeposito,@capital, (@valorCuota*@totalCuotas),@valorCuota,dateadd(month,1,@fechaPrestamo),dateadd(month,@totalCuotas,@fechaPrestamo),@totalCuotas,1,@tasa,(@valorCuota*@totalCuotas));
declare @nroPrestamo int;
set @nroPrestamo=(select max(NroPrestamo) from PRESTAMOS);
UPDATE DEPOSITOS SET RutCliente = @rut WHERE NroDeposito=@NroDeposito
UPDATE DEPOSITOS SET NroPrestamo = @nroPrestamo WHERE NroDeposito=@NroDeposito
/*=====================================================================================================*/
/*agrega registros a tabla cuotasPrestamo*/
declare @nroCuota int;
declare @fechaVencimiento date;
set @fechaVencimiento=dateadd(month,1,@fechaPrestamo);
set @nroCuota = 1;
WHILE @nroCuota < (@totalCuotas + 1)
BEGIN
	INSERT INTO CuotasPrestamo VALUES (@nroPrestamo, @nroCuota, @fechaVencimiento, @valorCuota,0);
	set @fechaVencimiento = dateadd(month,1,@fechaVencimiento);
	set @nroCuota = @nroCuota + 1;
END


/*ingreso de nuevo prestamo*/
declare @NroDeposito int;
declare @capital int;
declare @tasa decimal (4,2);
declare @totalCuotas int;
declare @rut varchar(15);
declare @valorCuota int;
set @NroDeposito=3;
set @capital = (select MontoDeposito from depositos where NroDeposito=@NroDeposito);
select @capital;
set @totalCuotas=12;
set @rut='66666666-6';
select clientes.nombrecliente, clientes.apellidocliente, clasificacion.tasa from clientes inner join clasificacion on clientes.tipoclasificacion=clasificacion.tipoclasificacion where clientes.rutcliente=@rut;
set @tasa=(select clasificacion.tasa from clientes inner join clasificacion on clientes.tipoclasificacion=clasificacion.tipoclasificacion where clientes.rutcliente=@rut);
select @tasa;
set @valorCuota=((@capital*(@tasa/100))*@totalCuotas);
set @valorCuota=(@valorCuota + @capital)/@totalCuotas;
select @valorCuota;
/*=====================================================================================================*/
/*agrega registro a tabla prestamos*/
/*ademas se actualiza tabla Depositos, los campos Rut Cliente y Nro Prestamo*/
declare @fechaPrestamo date = sysdatetime();
insert into PRESTAMOS values (@fechaPrestamo,@rut,@NroDeposito,@capital, (@valorCuota*@totalCuotas),@valorCuota,dateadd(month,1,@fechaPrestamo),dateadd(month,@totalCuotas,@fechaPrestamo),@totalCuotas,1,@tasa,(@valorCuota*@totalCuotas));
declare @nroPrestamo int;
set @nroPrestamo=(select max(NroPrestamo) from PRESTAMOS);
UPDATE DEPOSITOS SET RutCliente = @rut WHERE NroDeposito=@NroDeposito
UPDATE DEPOSITOS SET NroPrestamo = @nroPrestamo WHERE NroDeposito=@NroDeposito
/*=====================================================================================================*/
/*agrega registros a tabla cuotasPrestamo*/
declare @nroCuota int;
declare @fechaVencimiento date;
set @fechaVencimiento=dateadd(month,1,@fechaPrestamo);
set @nroCuota = 1;
WHILE @nroCuota < (@totalCuotas + 1)
BEGIN
	INSERT INTO CuotasPrestamo VALUES (@nroPrestamo, @nroCuota, @fechaVencimiento, @valorCuota,0);
	set @fechaVencimiento = dateadd(month,1,@fechaVencimiento);
	set @nroCuota = @nroCuota + 1;
END





 
/*=====================================================================================================*/
/*buscar cuotas a pagar*/
declare @rut varchar(15);
declare @nroCuota int;
declare @nroPrestamo int;
set @rut='44444444-4';
select NroPrestamo as 'Nro Prestamo', CuotaPendiente, ValorCuota from prestamos where RutCliente=@rut and saldo > 0 



/*buscar primera cuota a pagar*/
declare @rut varchar(15);
declare @nroCuota int;
declare @nroPrestamo int;
set @rut='44444444-4';
select top 1 NroPrestamo as 'Nro Prestamo', CuotaPendiente, ValorCuota from prestamos where RutCliente=@rut and saldo > 0 
set @nroCuota = (select top 1 CuotaPendiente from prestamos where RutCliente=@rut and saldo > 0)
set @nroPrestamo =  (select top 1 NroPrestamo from prestamos where RutCliente=@rut and saldo > 0)
select FechaVencimiento from CuotasPrestamo where nroPrestamo=@nroPrestamo and NroCuota=@nroCuota
/*=====================================================================================================*/




/*=====================================================================================================*/
/*ingreso de pago*/
declare @rut varchar(15);
declare @nroPrestamo int;
declare @nroCuota int;
declare @fechaPago date = sysdatetime();
declare @fechaVencimiento date;
declare @monto int;
set @rut='44444444-4';
set @nroPrestamo=1;
set @nroCuota=1;
set @fechaVencimiento='2019-01-19';
set @monto=41100;
INSERT INTO Pagos VALUES (@fechaPago,@rut, @nroPrestamo, @nroCuota, @fechaVencimiento, @monto);
UPDATE prestamos SET CuotaPendiente = CuotaPendiente + 1 WHERE NroPrestamo=@nroPrestamo
UPDATE prestamos SET saldo = saldo - @monto WHERE NroPrestamo=@nroPrestamo
UPDATE CuotasPrestamo SET SituacionCuota = 1 WHERE NroPrestamo=@nroPrestamo and NroCuota=@nroCuota 
   

select * from prestamos
select * from CuotasPrestamo
select * from Pagos

/*ingreso otro pago*/
declare @rut varchar(15);
declare @nroPrestamo int;
declare @nroCuota int;
declare @fechaPago date = sysdatetime();
declare @fechaVencimiento date;
declare @monto int;
set @rut='44444444-4';
set @nroPrestamo=1;
set @nroCuota=2;
set @fechaVencimiento='2019-02-19';
set @monto=41100;
INSERT INTO Pagos VALUES (@fechaPago,@rut, @nroPrestamo, @nroCuota, @fechaVencimiento, @monto);
UPDATE prestamos SET CuotaPendiente = CuotaPendiente + 1 WHERE NroPrestamo=@nroPrestamo
UPDATE prestamos SET saldo = saldo - @monto WHERE NroPrestamo=@nroPrestamo
UPDATE CuotasPrestamo SET SituacionCuota = 1 WHERE NroPrestamo=@nroPrestamo and NroCuota=@nroCuota 
   

select * from prestamos
select * from CuotasPrestamo
select * from Pagos

/*=====================================================================================================*/



/*=====================================================================================================*/
/*Actualizando Clasificacion Clientes*/
/*genera tabla comportamiento y Actualiza Tabla Clientes*/
/*=========================================================================*/

delete Comportamiento

declare @rut varchar (25) = '44444444-4'
declare @TipoClasificacion char = (select TipoClasificacion from Clientes where RutCliente=@Rut)
INSERT INTO comportamiento VALUES (@rut,0, 0, 0, 0,@TipoClasificacion);
UPDATE comportamiento SET TotalPrestamos = (select count(NroPrestamo) from prestamos where RutCliente=@rut) where RutCliente=@rut
UPDATE comportamiento SET TotalCuotas = (select sum(NroCuotas) from Prestamos where RutCliente=@rut and saldo>0) where RutCliente=@rut
UPDATE comportamiento SET CantidadCuotasPagadas = (select count(NroPago) from Pagos where RutCliente=@rut) where RutCliente=@rut
UPDATE comportamiento SET CantidadAtrasos = (select count(NroPago) from Pagos where RutCliente=@rut and FechaPago>FechaVencimiento) where RutCliente=@rut
UPDATE comportamiento SET TipoClasificacion = 'A' where (TotalPrestamos > 3 and CantidadCuotasPagadas > 0 and CantidadAtrasos = 0 and RutCliente=@rut)
UPDATE comportamiento SET TipoClasificacion = 'B' where (TotalPrestamos < 4 and CantidadCuotasPagadas > 0 and CantidadAtrasos = 0 and RutCliente=@rut)
UPDATE comportamiento SET TipoClasificacion = 'D' where (CantidadAtrasos > 0 and RutCliente=@rut)
set @TipoClasificacion = (select TipoClasificacion from COMPORTAMIENTO where RutCliente=@rut)
UPDATE Clientes SET TipoClasificacion = @TipoClasificacion where RutCliente=@rut





declare @rut varchar (25) = '55555555-5'
declare @TipoClasificacion char = (select TipoClasificacion from Clientes where RutCliente=@Rut)
INSERT INTO comportamiento VALUES (@rut,0, 0, 0, 0,@TipoClasificacion);
UPDATE comportamiento SET TotalPrestamos = (select count(NroPrestamo) from prestamos where RutCliente=@rut) where RutCliente=@rut
UPDATE comportamiento SET TotalCuotas = (select sum(NroCuotas) from Prestamos where RutCliente=@rut and saldo>0) where RutCliente=@rut
UPDATE comportamiento SET CantidadCuotasPagadas = (select count(NroPago) from Pagos where RutCliente=@rut) where RutCliente=@rut
UPDATE comportamiento SET CantidadAtrasos = (select count(NroPago) from Pagos where RutCliente=@rut and FechaPago>FechaVencimiento) where RutCliente=@rut
UPDATE comportamiento SET TipoClasificacion = 'A' where (TotalPrestamos > 3 and CantidadCuotasPagadas > 0 and CantidadAtrasos = 0 and RutCliente=@rut)
UPDATE comportamiento SET TipoClasificacion = 'B' where (TotalPrestamos < 4 and CantidadCuotasPagadas > 0 and CantidadAtrasos = 0 and RutCliente=@rut)
UPDATE comportamiento SET TipoClasificacion = 'D' where (CantidadAtrasos > 0 and RutCliente=@rut)
set @TipoClasificacion = (select TipoClasificacion from COMPORTAMIENTO where RutCliente=@rut)
UPDATE Clientes SET TipoClasificacion = @TipoClasificacion where RutCliente=@rut



declare @rut varchar (25) = '66666666-6'
declare @TipoClasificacion char = (select TipoClasificacion from Clientes where RutCliente=@Rut)
INSERT INTO comportamiento VALUES (@rut,0, 0, 0, 0,@TipoClasificacion);
UPDATE comportamiento SET TotalPrestamos = (select count(NroPrestamo) from prestamos where RutCliente=@rut) where RutCliente=@rut
UPDATE comportamiento SET TotalCuotas = (select sum(NroCuotas) from Prestamos where RutCliente=@rut and saldo>0) where RutCliente=@rut
UPDATE comportamiento SET CantidadCuotasPagadas = (select count(NroPago) from Pagos where RutCliente=@rut) where RutCliente=@rut
UPDATE comportamiento SET CantidadAtrasos = (select count(NroPago) from Pagos where RutCliente=@rut and FechaPago>FechaVencimiento) where RutCliente=@rut
UPDATE comportamiento SET TipoClasificacion = 'A' where (TotalPrestamos > 3 and CantidadCuotasPagadas > 0 and CantidadAtrasos = 0 and RutCliente=@rut)
UPDATE comportamiento SET TipoClasificacion = 'B' where (TotalPrestamos < 4 and CantidadCuotasPagadas > 0 and CantidadAtrasos = 0 and RutCliente=@rut)
UPDATE comportamiento SET TipoClasificacion = 'D' where (CantidadAtrasos > 0 and RutCliente=@rut)
set @TipoClasificacion = (select TipoClasificacion from COMPORTAMIENTO where RutCliente=@rut)
UPDATE Clientes SET TipoClasificacion = @TipoClasificacion where RutCliente=@rut

select * from comportamiento
SELECT * FROM CLIENTES
/*=========================================================================*/

/*=========================================================================*/
/*Prestamistas recibiran dinero mas tasa interes pactada por deposito*/
declare @rut varchar(15)
declare @total int

set @rut = '11111111-1'
set @total=(select (depositos.MontoDeposito*(1 + (depositos.tasaInteres/100))) from depositos where rutprestamista=@rut)
select prestamistas.nombrePrestamista, depositos.MontoDeposito, @total as 'Total que Recibira' from depositos inner join prestamistas on depositos.RutPrestamista=prestamistas.RutPrestamista where depositos.rutprestamista=@rut

set @rut = '22222222-2'
set @total=(select (depositos.MontoDeposito*(1 + (depositos.tasaInteres/100))) from depositos where rutprestamista=@rut)
select prestamistas.nombrePrestamista, depositos.MontoDeposito, @total as 'Total que Recibira' from depositos inner join prestamistas on depositos.RutPrestamista=prestamistas.RutPrestamista where depositos.rutprestamista=@rut

set @rut = '33333333-3'
set @total=(select (depositos.MontoDeposito*(1 + (depositos.tasaInteres/100))) from depositos where rutprestamista=@rut)
select prestamistas.nombrePrestamista, depositos.MontoDeposito, @total as 'Total que Recibira' from depositos inner join prestamistas on depositos.RutPrestamista=prestamistas.RutPrestamista where depositos.rutprestamista=@rut

/*=========================================================================*/
/*=========================================================================*/

/*prestamistas quieren saber a quien se la ha prestado su dinero*/
select prestamistas.RutPrestamista, prestamistas.NombrePrestamista, depositos.RutCliente, clientes.nombrecliente, clientes.apellidocliente, depositos.NroPrestamo, prestamos.MOntoCapital, prestamos.NroCuotas, prestamos.ValorCuota, (prestamos.CuotaPendiente - 1) as 'Ultima Cuota Pagada' from prestamistas inner join depositos on prestamistas.RutPrestamista=depositos.RutPrestamista inner join prestamos on depositos.NroPrestamo=prestamos.NroPrestamo inner join clientes on depositos.RutCliente=clientes.RutCliente


/*=========================================================================*/
/*=========================================================================*/

/*prestamistas quieren saber fechas que recibiran pagos*/

select CuotasPrestamo.NroPrestamo, CuotasPrestamo.NroCuota, CuotasPrestamo.FechaVencimiento, CuotasPrestamo.MOntoCuota from CuotasPrestamo where CuotasPrestamo.SituacionCuota=0
select CuotasPrestamo.FechaVencimiento, sum(CuotasPrestamo.MOntoCuota) as 'Monto' from CuotasPrestamo where CuotasPrestamo.SituacionCuota=0 group by CuotasPrestamo.FechaVencimiento


/*=========================================================================*/
/*=========================================================================*/

/*prestamistas quieren saber perfil de clientes*/

select * from Clientes


/*=========================================================================*/
/*=========================================================================*/

/*Yo como prestatario quiero consultar los montos que debo*/





/*=========================================================================*/
/*=========================================================================*/

/*Yo como prestatario quiero consultar las cuotas que pagado y las que aun me quedan por pagar*/






/*=========================================================================*/
/*=========================================================================*/

/*Yo como prestatario quiero consultar el perfil de las personas que me han prestado (perfil de cliente)*/
       


